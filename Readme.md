# Cambio de Monedas

Niveles por resolver:

Nivel 1. Realizar una aplicaci�n Android para la conversi�n de Bolivianos a diferentes
otros tipos de Monedas. En el cuadro de abajo se muestra los tipos de cambio por cada
tipo de moneda. Para este nivel, s�lo introduce BOLIVIANOS y al presionar CONVERTIR
realiza los c�lculos e imprime las conversiones.

Nivel 2. Agregar un bot�n para Inicializar los resultados y el monto en bolivianos
introducido.

Nivel 3. Modificar el programa para que solamente acepte un monto en cualquiera de
las Monedas, y realice los c�lculos e impresi�n de los equivalentes en las otras
monedas
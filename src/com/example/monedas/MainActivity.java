package com.example.monedas;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void procesar(View vista){
		EditText p0 =(EditText) findViewById(R.id.editText1);
		int bolivianos = Integer.parseInt(p0.getText().toString());
		
		float m7= (float) ( bolivianos/6.96);
		float m6= (float) ( bolivianos/7.52);
		float m5= (float) ( bolivianos/2.23);
		float m4= (float) ( bolivianos/0.011354);
		float m3= (float) ( bolivianos/2.2852);
		float m2= (float) ( bolivianos/1.1379);
		float m1= (float) ( bolivianos/0.05849034);
		
		EditText p7 = (EditText) findViewById(R.id.EditText07);
		EditText p6 = (EditText) findViewById(R.id.EditText06);
		EditText p5 = (EditText) findViewById(R.id.EditText05);
		EditText p4 = (EditText) findViewById(R.id.EditText04);
		EditText p3 = (EditText) findViewById(R.id.EditText03);
		EditText p2 = (EditText) findViewById(R.id.EditText02);
		EditText p1 = (EditText) findViewById(R.id.EditText01);
		p7.setText(m7+"");
		p6.setText(m6+"");
		p5.setText(m5+"");
		p4.setText(m4+"");
		p3.setText(m3+"");
		p2.setText(m2+"");
		p1.setText(m1+"");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
